define(["jquery", "backbone", "text!views/templates/industries.html"],function($,Backbone,IndustriesTemplate){
	
	var IndustriesView = Backbone.View.extend({
		attributes : {
			'data-role' : 'page'
		},

		template : _.template(IndustriesTemplate),

		render : function(eventName) {
			$(this.el).html(this.template(this.model));
			return this;
		},
		
		events : {
			"click .box-wrapper" : "selectIndustry",
		},
		
		selectIndustry : function(event){
			console.log($(event.target));
			var selectedIndustryId
			if(($(event.target).attr('class')!="box") || ($(event.target).attr('class')!="box-wrapper"))
			{
				console.log($(event.target).closest("div").attr('id'));
				selectedIndustryId = $(event.target).closest("div").attr('id');
			}
			else{
				console.log($(event.target).attr('id'));
				selectedIndustryId = $(event.target).attr('id');
			};
			location.href="#/categories/"+selectedIndustryId;
			//router.categories(selectedIndustryId);
			return false;
		}
	});
	return IndustriesView;
});