define(["jquery", "backbone", "text!views/templates/dimensions.html", "common"], function($, Backbone, DimensionsTemplate, Common) {

	var DimensionsView = Backbone.View.extend({
		

		attributes : {
			'data-role' : 'page'
		},

		template : _.template(DimensionsTemplate),

		render : function(eventName) {
			$(this.el).html(this.template(this.model));
			
			return this;
		},
		
		
		events : {
			"click #submitDimension" : "submitDimensions",
		},
		
		submitDimensions : function(event) {
			router.selectedDimension = {};
			router.selectedDimension.category = $('#line-dropdown').find('.menuSelection').text().trim();
			router.selectedDimension.categoryId = $('#line-dropdown').find('.menuSelection').attr('id');
			router.selectedDimension.distributionChannel = $('#channel-dropdown').find('.menuSelection').text().trim();
			if(router.selectedDimension.distributionChannel==="All"){
				router.selectedDimension.distributionChannelId = "all";
			}else{
				router.selectedDimension.distributionChannelId = $('#channel-dropdown').find('.menuSelection').attr('id');
			}
			
			router.selectedDimension.geography = $('#geography-dropdown').find('.menuSelection').text().trim();
			router.selectedDimension.geographyId = $('#geography-dropdown').find('.menuSelection').attr('id');
			router.selectedDimension.period = $('#period-dropdown').find('.menuSelection').text().trim();
			router.selectedDimension.periodId = $('#period-dropdown').find('.menuSelection').attr('id');
			if(router.selectedDimension.geographyId==undefined){
				for(var count=0; count<dimensionResponseArray.dimensions[2].dimensionInfo.length; count++){
					if(dimensionResponseArray.dimensions[2].dimensionInfo[count].name=="India"){
						router.selectedDimension.geographyId = dimensionResponseArray.dimensions[2].dimensionInfo[count].id;
					}
				}
			}
			
			if(router.selectedDimension.periodId==undefined){
				/* for(var count=0; count<dimensionResponseArray.dimensions[3].dimensionInfo.length; count++){
					if(dimensionResponseArray.dimensions[3].dimensionInfo[count].year==new Date().getFullYear()){
						router.selectedDimension.periodId = dimensionResponseArray.dimensions[3].dimensionInfo[count].year;
					}
				} */
				router.selectedDimension.periodId = new Date().getFullYear();
			}
			console.log("dimensionsArray ---> " + dimensionResponseArray)
			router.getDriverTree($('#line-dropdown').find('.menuSelection').attr('id'), $('#channel-dropdown').find('.menuSelection').attr('id'), router.selectedDimension.geographyId, router.selectedDimension.periodId);
			
			//location.href="#/driverTree/Planning";
			return false;
		}
	});
	return DimensionsView;
});