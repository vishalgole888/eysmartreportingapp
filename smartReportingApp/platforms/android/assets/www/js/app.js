define(["backbone","router/router"],function(Backbone, AppRouter){
	
	return {
		
		initialize : function(){
			//alert("app initialize");
			router = new AppRouter();
			if(window.localStorage.getItem("userToken")==""||window.localStorage.getItem("userToken")==undefined||window.localStorage.getItem("userToken")==null){
				window.localStorage.setItem("userToken","8EsQEZwEgbxgAZ3tj8xE");
				//window.localStorage.setItem("userToken","b_38i-WRyPL5AkWkEVvS");
				
			}
			document.addEventListener("backbutton", function(e){
			   if(window.location.hash==''){
			    	//alert(window.location.hash);
			       //alert("Back Button Pressed");
			       
			         if (confirm('Are you sure you want exit from application?')) {
			            // // Save it! 
			        	  e.preventDefault();
			        	 navigator.app.exitApp();
			         } else {
			            // // Do nothing!
			        }
			       
			     }
			    else {
			    	history.go(-1);
			       // navigator.app.backHistory();
			     }
			    
			   
			}, false);
			 window.addEventListener('orientationchange', function(e){
				 
				 window.screen.lockOrientation('portrait');
				 //alert("screen orientationchange");
				 /* if(window.location.hash==="#/driverTree/Planning"){
					 return true;
				 }
					 
				 else
					 Screen.lockOrientation('portrait'); */
				 
			 });
		}
	};
});
