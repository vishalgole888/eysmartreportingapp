define(["jquery", "backbone", "views/sectorsView", "views/industriesView", "views/categoriesView", "views/performanceMetricsView", "views/operationsView", "views/dimensionsView", "views/driverTreeView", "service/service", "service/requestBuilder", "fg-menu"], function($, Backbone, SectorsView, IndustriesView, CategoriesView, PerformanceMetricsView, OperationsView, DimensionsView, DriverTreeView, Service, RequestBuilder) {
	var sectorId;
	var industryId;
	var categoryId;
	var metricId;
	var analysis;
	var lastFivePeriods = [];
	var operationSelected;
	var selectedDimension = {};
	selectedPeriodParameterValue = '';
	var selectedGeographyParameter = "";
	var driverTreeResponse = [];
	var earlierParentId = "";
	var tradeVal = 0;
	var cbbVal = 0;
	var exportVal = 0;
	var modernTradeVal = 0;
	var piechartValsArray = [];
	var piechartValPercentage = [];
	var AppRouter = Backbone.Router.extend({
		routes : {
			"" : "sectors",
			"/" : "sectors",
			"sectors" : "sectors",
			"industries/:sectorId" : "industries",
			"categories/:industryId" : "categories",
			"performanceMetrics/:categoryId" : "performanceMetrics",
			"operations/:metricId" : "operations",
			"dimensions/:operation" : "dimensions",
			"driverTree/:operationSelected" : "driverTree"
		},

		initialize : function() {
			router = AppRouter;
			_.bindAll.apply(_, [this].concat(_.functions(this)));
			Backbone.history.start();

		},
		changePage : function(page) {
			console.log("page is");
			console.log(page);
			page.render();
			$('.main').html($(page.el));
			//alert(window.location.hash);
		},
		sectors : function() {
			//alert("Home");
			$('#loading').show();
			Service.getSectors(new RequestBuilder.getSectorsRequest(), this.onGetSectorsSucess, this.onGetSectorsError);

		},
		onGetSectorsSucess : function(response) {
			console.log(response);
			this.changePage(new SectorsView({
				model : {
					sectors : response.data,
				}
			}));
			$('#loading').hide();
		},
		onGetSectorsError : function(response) {
			console.log(response);
			$('#loading').hide();
		},
		industries : function(id) {
			console.log("SectorId : " + id);
			sectorId = id;
			$('#loading').show();
			Service.getIndustries(new RequestBuilder.getIndustriesRequest(id), this.onGetIndustriesSuccess, this.onGetIndustriesError);

		},
		onGetIndustriesSuccess : function(response) {
			console.log(response.data);
			this.changePage(new IndustriesView({
				model : {
					industries : response.data,
				}
			}));
			$('#loading').hide();
		},
		onGetIndustriesError : function(response) {
			console.log(response);
		},
		categories : function(id) {
			industryId = id;
			console.log("IndustryId : " + id);
			$('#loading').show();
			Service.getCategories(new RequestBuilder.getPerformanceMetricCategory(id), this.onGetCategorySuccess, this.onGetCategoryError);

		},
		onGetCategorySuccess : function(response) {
			console.log(response.data);
			this.changePage(new CategoriesView({
				model : {
					categories : response.data,
				}
			}));
			$('#loading').hide();
		},
		onGetCategoryError : function(response) {
			$('#loading').hide();
		},
		performanceMetrics : function(id) {
			categoryId = id;
			console.log("category id : " + id);
			$('#loading').show();
			Service.getPerformanceMetrics(new RequestBuilder.getPerformanceMetric(id), this.onGetPerformanceMetricSuccess, this.onGetPerformanceMetricError);

		},
		onGetPerformanceMetricSuccess : function(response) {
			console.log(response.data);
			this.changePage(new PerformanceMetricsView({
				model : {
					metrics : response.data,
				}
			}));
			$('#loading').hide();
		},
		onGetPerformanceMetricError : function(response) {
			console.log(response);
			$('#loading').hide();
		},
		operations : function(id) {
			metricId = id;
			this.changePage(new OperationsView());
		},
		dimensions : function(operationName) {
			$('#loading').show();
			operationSelected = operationName;
			Service.getDimensions(new RequestBuilder.getDimensions(metricId, operationName), this.onGetDimensionSuccess, this.onGetDimensionError);

		},
		performanceAnalysis : function() {
			$('#loading').show();
			Service.getPerformanceAnalysis(new RequestBuilder.getPerformanceAnalysis(metricId), this.onGetPerformanceSuccess, this.onGetPerformanceError);

		},
		checkVal : function(val){
			if(val==undefined || val == null){
				val = 0;
			}
			return val;
		},
		onGetPerformanceSuccess : function(response) {
			//alert("got performance success");
			console.log(response.data);
			debugger;
			tradeVal = response.data.analysis.performanceMetrics[1].actual_value;
			tradeVal = this.checkVal(tradeVal);
			cbbVal = response.data.analysis.performanceMetrics[2].actual_value;
			cbbVal=this.checkVal(cbbVal);
			exportVal = response.data.analysis.performanceMetrics[3].actual_value;
			exportVal = this.checkVal(exportVal);
			modernTradeVal = response.data.analysis.performanceMetrics[4].actual_value;
			modernTradeVal = this.checkVal(modernTradeVal);
			piechartValsArray = [tradeVal,cbbVal,exportVal,modernTradeVal];
			//piechartValPercentage=this.toPctFn(piechartValsArray);
			/* for(var count=0; count < piechartValPercentage.length; count++){
				//alert("percentage value is -->" + piechartValPercentage[count]);
			} */
			analysis = response.data.analysis;
			var dimensions = response.data.dimensions;
			productCategory = dimensions.dimensions[0];
			distributionChannel = dimensions.dimensions[1];
			geography = dimensions.dimensions[2];
			period = dimensions.dimensions[3];
			if(operationSelected === "tab3"){
				operationSelected = "Performanceanalysis";
			}
			if (operationSelected == "Performanceanalysis") {
				location.href = "#/driverTree/" + operationSelected;
			}

			$('.metricName').text(analysis.performanceMetrics['All'].name + " (Cr.)");
			var actualValue = parseFloat(analysis.performanceMetrics["All"].actual_value);
			$('.actualValue').text(Math.ceil(actualValue));
			var txt = analysis.performanceMetrics["All"].planned_value;
			var numb = txt.match(/\d/g);
			numb = numb.join("");
			$('.plannedValue').text(Math.ceil(numb));
			$('.variationValue').text(analysis.performanceMetrics["All"].variation);

			if (router.selectedDimension == undefined) {

				var lastFiveYears = [];
				for (var i = 0; i <= 4; i++) {
					lastFiveYears.push(parseInt(new Date().getFullYear()) - i);
				}
				lastFivePeriods = lastFiveYears;
			} else {
				selectedPeriodParameterValue = 'year';
				var lastFiveYears = [];
				for (var i = 0; i <= 4; i++) {
					if(router.selectedDimension.period!="Select period")
					lastFiveYears.push(parseInt(router.selectedDimension.period) - i);
				else
					lastFiveYears.push(parseInt(new Date().getFullYear()) - i);
				}
				lastFivePeriods = lastFiveYears;
			}
			this.updateBarGraph();
			this.updatePieChart();
			$('#loading').hide();
		},
		onGetPerformanceError : function(response) {
			console.log(response);
			$('#loading').hide();
		},
		onGetDimensionSuccess : function(response) {
			console.log(response.data);
			dimensionResponseArray = [];
			dimensionResponseArray = response.data;
			productCategory = response.data.dimensions[0];
			distributionChannel = response.data.dimensions[1];
			geography = response.data.dimensions[2];
			period = response.data.dimensions[3];

			productCategoryKey = response.data.dimensions[0].dimensionName;
			distributionChannelKey = response.data.dimensions[1].dimensionName;
			geographyKey = response.data.dimensions[2].dimensionName;
			periodKey = response.data.dimensions[3].dimensionName;

			if (operationSelected == 'Performanceanalysis') {
				location.href = "#/driverTree/" + operationSelected;
				//Service.getPerformanceAnalysis(new RequestBuilder.getPerformanceAnalysis(metricId), this.onGetPerformanceSuccess, this.onGetPerformanceError);
			} else {
				operationSelected = "Planning";
				this.changePage(new DimensionsView({
					model : {
						productCategories : productCategory.dimensionInfo,
						distributionChannels : distributionChannel.dimensionInfo,
						geographies : geography.dimensionInfo,
						periods : period.dimensionInfo
					}
				}));
			}
			dropdown_function();
			$('#loading').hide();

		},
		onGetDimensionError : function(response) {
			console.log(response);
			$('#loading').hide();
		},
		changeInDropDown : function(selectedParameter, selectedItem, selectedItemId) {
			console.log(selectedParameter + " : " + selectedItem + " : " + selectedItemId);
			//lastFivePeriods = [];
			//alert(selectedDimension);
			//alert(selectedItem);
			if (router.selectedDimension == undefined) {
				router.selectedDimension = {};

			}
			if (selectedParameter == "product_category") {

				router.selectedDimension.category = selectedItem;
				router.selectedDimension.categoryId = selectedItemId;
			} else if (selectedParameter == "distribution_channel") {
				router.selectedDimension.distributionChannel = selectedItem;
				router.selectedDimension.distributionChannelId = selectedItemId;
			} else if (selectedParameter == "country" || selectedParameter == "state" || selectedParameter == "city") {

				selectedGeographyParameter = selectedParameter;
				router.selectedDimension.geography = selectedItem;
				router.selectedDimension.geographyId = selectedItemId;
			}

			if (selectedParameter == 'quarter') {
				selectedPeriodParameterValue = 'quarter';
				var selectedQuarter = selectedItem.split("_")[0];
				var selectedYear = selectedItem.split("_")[1];
				var lastFiveQuarters = [];
				if (selectedQuarter == 'Q4') {
					lastFiveQuarters.push("Q4_" + selectedYear, "Q3_" + selectedYear, "Q2_" + selectedYear, "Q1_" + selectedYear, "Q4_" + (parseInt(selectedYear) - 1));
					//, ,   );
				} else if (selectedQuarter == 'Q3') {
					lastFiveQuarters.push("Q3_" + selectedYear, "Q2_" + selectedYear, "Q1_" + selectedYear, "Q4_" + (parseInt(selectedYear) - 1), "Q3_" + (parseInt(selectedYear) - 1));
				} else if (selectedQuarter == 'Q2') {
					lastFiveQuarters.push("Q2_" + selectedYear, "Q1_" + selectedYear, "Q4_" + (parseInt(selectedYear) - 1), "Q3_" + (parseInt(selectedYear) - 1), "Q2_" + (parseInt(selectedYear) - 1));
				} else if (selectedQuarter == 'Q1') {
					lastFiveQuarters.push("Q1_" + selectedYear, "Q4_" + (parseInt(selectedYear) - 1), "Q3_" + (parseInt(selectedYear) - 1), "Q2_" + (parseInt(selectedYear) - 1), "Q1_" + (parseInt(selectedYear) - 1));
				}
				console.log(lastFiveQuarters);
				lastFivePeriods = lastFiveQuarters;
			} else if (selectedParameter == 'month') {
				selectedPeriodParameterValue = 'month';
				var selectedMonth = selectedItem.split("_")[0];
				var selectedYear = selectedItem.split("_")[1];
				var lastFiveMonths = [];
				var lastDateOfMonth = new Date(selectedYear, selectedMonth - 1);
				console.log(lastDateOfMonth);
				for (var i = 0; i <= 4; i++) {
					var date = new Date(lastDateOfMonth);
					var lastMonth = lastDateOfMonth.getMonth();
					var dateforMonth = new Date(date.setMonth((lastDateOfMonth.getMonth() - i)));
					console.log(date);
					lastFiveMonths.push((dateforMonth.getMonth() + 1) + "_" + dateforMonth.getFullYear());

				}
				lastFivePeriods = lastFiveMonths;

				// var fiveMonths = new Date(lastDateOfMonth.setMonth(lastDateOfMonth.getMonth() - 5));
				// console.log(fiveMonths);

			} else if (selectedParameter == 'year') {
				selectedPeriodParameterValue = 'year';
				var lastFiveYears = [];
				for (var i = 0; i <= 4; i++) {
					lastFiveYears.push(selectedItem - i);
				}
				lastFivePeriods = lastFiveYears;
			}
			//alert(operationSelected);
			if(operationSelected == "Planning"){
				router.getDriverTree(router.selectedDimension.categoryId, router.selectedDimension.distributionChannelId, router.selectedDimension.geographyId, $('#period-dropdown').find('.menuSelection').text().trim());
			}
			this.updateBarGraph();
			this.updatePieChart();
			
		},
		driverTree : function(operationSelected) {

			if (operationSelected == "Planning") {
				console.log(this.selectedDimension);
				console.log(productCategory);
				this.changePage(new DriverTreeView({
					model : {
						analysisData : null,
						productCategories : productCategory.dimensionInfo,
						distributionChannels : distributionChannel.dimensionInfo,
						geographies : geography.dimensionInfo,
						periods : period.dimensionInfo.reverse(),
						selectedCategory : selectedCategoryName,
						selectedPerformanceMetrics : selectedPerformanceMetrics,

					}
				}));
				dropdown_function();

				//UPDATE THE DROPDOWN VALUES AS PER SELECTION

				$('#line-dropdown').find('.menuSelection').attr('id', this.selectedDimension.categoryId);
				$('#line-dropdown').find('.menuSelection').text(this.selectedDimension.category);
				$('#channel-dropdown').find('.menuSelection').attr('id', this.selectedDimension.distributionChannelId);
				$('#channel-dropdown').find('.menuSelection').text(this.selectedDimension.distributionChannel);
				$('#geography-dropdown').find('.menuSelection').attr('id', this.selectedDimension.geographyId);
				$('#geography-dropdown').find('.menuSelection').text(this.selectedDimension.geography);
				$('#period-dropdown').find('.menuSelection').text(this.selectedDimension.period);

				
				router.generateDriverTreeNode(driverTreeResponse, "N1");

			} else if (operationSelected == "Performanceanalysis") {

				this.changePage(new DriverTreeView({
					model : {

						productCategories : productCategory.dimensionInfo,
						distributionChannels : distributionChannel.dimensionInfo,
						geographies : geography.dimensionInfo,
						periods : period.dimensionInfo,
						selectedCategory : selectedCategoryName,
						selectedPerformanceMetrics : selectedPerformanceMetrics
					}
				}));

				dropdown_function();
				selectedPeriod = new Date().getFullYear();
				selectedPeriodParameterValue = 'year';
				for (var i = 0; i <= 4; i++) {
					period = parseInt(selectedPeriod) - i;
					lastFivePeriods.push(period);
				}
				var arrangeArr = [];
				for(var cnt=0; cnt < lastFivePeriods.length; cnt++){
					if(!(isNaN(lastFivePeriods[cnt]))){
						if(arrangeArr.length < 5)
						arrangeArr.push(lastFivePeriods[cnt]);
					}
				}
				lastFivePeriods = [];
				lastFivePeriods = arrangeArr;
				$('.tab3').trigger('click', function() {
					console.log("hii");
				});

			}
			/* if(document.getElementById("N1")){
				document.getElementById("N1").panzoom();
			} */
			if(document.getElementById("N1_span")){
				var ele = document.getElementById("N1_span"); 
				window.scrollTo(ele.offsetLeft,(ele.offsetTop-50));
			}
			
		},

		getDriverTree : function(productCatergory, distributionChannel, geography, period) {
			$('#loading').show();
			if(period==="Select period"){
				period= new Date().getFullYear();
			}
			
			if(window.location.hash==="#/dimensions/Planning"){
				if(!(event.target.parentElement.hasAttribute("role"))){
					Service.getDriverTree(new RequestBuilder.getDriverTree(metricId, productCatergory, distributionChannel, geography, period), this.getDriverTreeSuccess, this.getDriverTreeError);
				}else{
					$('#loading').hide();
				}
			}else{
				Service.getDriverTree(new RequestBuilder.getDriverTree(metricId, productCatergory, distributionChannel, geography, period), this.getDriverTreeSuccess, this.getDriverTreeError);
			}
			
			
		},
		getDriverTreeSuccess : function(response) {
			//alert(response.data[0].name);
			console.log(response.data);
			console.log(operationSelected);
			if (operationSelected == "Performanceanalysis") {
				location.href = "#/driverTree/" + operationSelected;
			} else {
				location.href = "#/driverTree/Planning";
				//driverTreeRootNode = {};
				//driverTreeRootNode = response.data[0];
				driverTreeResponse = [];
				driverTreeResponse = response.data[0];
			}

			$('#loading').hide();
		},
		getDriverTreeError : function(response) {
			console.log(response);
			alert("Please select all the dimensions");
			$('#loading').hide();
		},
		updateBarGraph : function() {
			//if (operationSelected == 'performanceAnalysis') {
			var actualValues = [];
			var planValues = [];
			actualValueCollection = [];
			planValueCollection = [];
			if(analysis==undefined){
				this.performanceAnalysis();
			}
			var collection = analysis.graph_data;
			console.log(selectedPeriodParameterValue);
			console.log(this.selectedDimension);
			for (var i = 0; i <= 4; i++) {
				if (router.selectedDimension != undefined) {
					//alert("SelectedDimension defined");
					if (router.selectedDimension.category != undefined && router.selectedDimension.distributionChannel != undefined) {
						//alert("Category and DC defined");
						actualValueCollection = collection.filter(function(object) {
							//console.log(object.year + " : " + object.product_category + " : "+ object.distribution_channel);
							console.log(lastFivePeriods[i] + " : " + router.selectedDimension.category + " : " + router.selectedDimension.distributionChannel);
							if (selectedPeriodParameterValue == 'year') {

								return ((object.year == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && (selectedGeographyParameter == selectedDimension.geography));
							} else if (selectedPeriodParameterValue == 'quarter') {
								return ((object.quarter == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							} else if (selectedPeriodParameterValue == 'month') {
								return ((object.month == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							}
						});
						planValueCollection = collection.filter(function(object) {
							if (selectedPeriodParameterValue == 'year') {
								return ((object.year == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							} else if (selectedPeriodParameterValue == 'quarter') {
								return ((object.quarter == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							} else if (selectedPeriodParameterValue == 'month') {
								console.log(object.month);
								return ((object.month == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							}
						});
					} else if (router.selectedDimension.category == undefined && router.selectedDimension.distributionChannel != undefined) {
						//alert("Category undefined");
						actualValueCollection = collection.filter(function(object) {
							//console.log(object.year + " : " + object.product_category + " : "+ object.distribution_channel);
							//console.log(lastFivePeriods[i] + " : " + router.selectedDimension.category + " : " + router.selectedDimension.distributionChannel);
							if (selectedPeriodParameterValue == 'year') {

								return ((object.year == lastFivePeriods[i]) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && (selectedGeographyParameter == selectedDimension.geography));
							} else if (selectedPeriodParameterValue == 'quarter') {
								return ((object.quarter == lastFivePeriods[i]) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							} else if (selectedPeriodParameterValue == 'month') {
								return ((object.month == lastFivePeriods[i]) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							}
						});
						planValueCollection = collection.filter(function(object) {
							if (selectedPeriodParameterValue == 'year') {
								return ((object.year == lastFivePeriods[i]) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							} else if (selectedPeriodParameterValue == 'quarter') {
								return ((object.quarter == lastFivePeriods[i]) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							} else if (selectedPeriodParameterValue == 'month') {
								console.log(object.month);
								return ((object.month == lastFivePeriods[i]) && (object.distribution_channel == router.selectedDimension.distributionChannel));
								// && selectedGeographyParameter == selectedDimension.geography;
							}
						});
					} else if (router.selectedDimension.category != undefined && router.selectedDimension.distributionChannel == undefined) {
						//alert("DC undefined");
						actualValueCollection = collection.filter(function(object) {
							//console.log(object.year + " : " + object.product_category + " : "+ object.distribution_channel);
							//console.log(lastFivePeriods[i] + " : " + router.selectedDimension.category + " : " + router.selectedDimension.distributionChannel);
							if (selectedPeriodParameterValue == 'year') {

								return ((object.year == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category));
								// && (selectedGeographyParameter == selectedDimension.geography));
							} else if (selectedPeriodParameterValue == 'quarter') {
								return ((object.quarter == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category));
								// && selectedGeographyParameter == selectedDimension.geography;
							} else if (selectedPeriodParameterValue == 'month') {
								return ((object.month == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category));
								// && selectedGeographyParameter == selectedDimension.geography;
							}
						});
						planValueCollection = collection.filter(function(object) {
							if (selectedPeriodParameterValue == 'year') {
								return ((object.year == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category));
								// && selectedGeographyParameter == selectedDimension.geography;
							} else if (selectedPeriodParameterValue == 'quarter') {
								return ((object.quarter == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category));
								// && selectedGeographyParameter == selectedDimension.geography;
							} else if (selectedPeriodParameterValue == 'month') {
								console.log(object.month);
								return ((object.month == lastFivePeriods[i]) && (object.product_category == router.selectedDimension.category));
								// && selectedGeographyParameter == selectedDimension.geography;
							}
						});
					}
				} else {
					//alert("SelectedDimension defined");
					actualValueCollection = collection.filter(function(object) {
						if (selectedPeriodParameterValue == 'year') {

							return (object.year == lastFivePeriods[i])
							// && (selectedGeographyParameter == selectedDimension.geography));
						} else if (selectedPeriodParameterValue == 'quarter') {
							return (object.quarter == lastFivePeriods[i]);
							// && selectedGeographyParameter == selectedDimension.geography;
						} else if (selectedPeriodParameterValue == 'month') {
							return (object.month == lastFivePeriods[i]);
							// && selectedGeographyParameter == selectedDimension.geography;
						}
					});
					planValueCollection = collection.filter(function(object) {
						if (selectedPeriodParameterValue == 'year') {
							return (object.year == lastFivePeriods[i]);
							// && selectedGeographyParameter == selectedDimension.geography;
						} else if (selectedPeriodParameterValue == 'quarter') {
							return (object.quarter == lastFivePeriods[i]);
							// && selectedGeographyParameter == selectedDimension.geography;
						} else if (selectedPeriodParameterValue == 'month') {
							console.log(object.month);
							return (object.month == lastFivePeriods[i]);
							// && selectedGeographyParameter == selectedDimension.geography;
						}
					});
				}
				console.log(collection);
				console.log(actualValueCollection);
				console.log(planValueCollection);
				var actualtotal = 0;
				var planTotal = 0;
				$.each(actualValueCollection, function() {
					actualtotal += parseFloat(this.actual_value);
				});
				$.each(planValueCollection, function() {
					planTotal += parseFloat(this.planned_value);
				});
				actualtotal = Math.ceil(actualtotal);
				planTotal = Math.ceil(planTotal);
				actualValues.push(actualtotal);
				planValues.push(planTotal);

			}
			//actualValues.reverse();
			//planValues.reverse();
			//lastFivePeriods.reverse();

			$('#revenue-line-graph').highcharts({
				credits : {
					enabled : false
				},
				chart : {
					zoomType : 'xy'
				},
				title : {
					text : ''
				},
				xAxis : [{
					categories : lastFivePeriods,
					crosshair : true
				}],
				yAxis : [{// Primary yAxis
					labels : {

						format : '{value}',
						style : {
							color : Highcharts.getOptions().colors[1]
						}
					},
					title : {
						text : '',
						style : {
							color : Highcharts.getOptions().colors[1]
						}
					}
				}, {// Secondary yAxis
					title : {
						text : '',
						style : {
							color : Highcharts.getOptions().colors[1]
						}
					},
					labels : {
						format : '{value}',
						style : {
							color : Highcharts.getOptions().colors[1]
						}
					},
					opposite : true
				}],
				tooltip : {
					shared : true
				},
				legend : {
					backgroundColor : '#fff',
					borderColor : '#ddd',
					borderWidth : 1,
					itemDistance : 50,
					fontWeight : 'bold'
				},
				series : [{
					name : 'Actual',
					type : 'column',
					yAxis : 1,
					data : actualValues,
					tooltip : {
						valueSuffix : 'Rs. Cr'
					},
					color : "#808080"

				}, {
					name : 'Plans',
					type : 'spline',
					data : planValues,
					tooltip : {
						valueSuffix : 'Rs. Cr'
					},
					color : "#ffd200"
				}]
			});
			//}
		},

		updatePieChart : function() {
			//if (operationSelected == 'performanceAnalysis') {
			var collection = analysis.graph_data;
			productOneCollection = collection.filter(function(object) {
				return object.year == '2015' && object.product_category == 'Product 1';
			});
			var prodOneTotal = 0;
			var prodTwoTotal = 0;
			$.each(productOneCollection, function() {
				prodOneTotal += parseFloat(this.actual_value);
			});
			productTwoCollection = collection.filter(function(object) {
				return object.year == '2015' && object.product_category == 'Product 2';
			});
			$.each(productTwoCollection, function() {
				prodTwoTotal += parseFloat(this.actual_value);
			});
			$('#revenue-pie-business-graph').highcharts({
				credits : {
					enabled : false
				},
				chart : {
					type : 'pie',
					options3d : {
						enabled : false,
						alpha : 45
					}
				},
				title : {
					text : ''
				},
				subtitle : {
					text : ''
				},
				plotOptions : {
					pie : {
						innerSize : 100,
						depth : 45,
						dataLabels : {
							enabled : false
						},
						showInLegend : true
					},

				},
				legend : {
					align : 'right',
					verticalAlign : 'top',
					y : 60,
					layout : 'vertical',
					padding : 3,
					itemMarginTop : 5,
					itemMarginBottom : 5,
				},
				series : [{
					name : 'Product',
					data : [['Product 1', prodOneTotal], ['Product 2', prodTwoTotal]]
				}]
			});
			$('#revenue-pie-distribustion-graph').highcharts({
				credits : {
					enabled : false
				},
				chart : {
					type : 'pie',
					options3d : {
						enabled : false,
						alpha : 45
					}
				},

				title : {
					text : ''
				},
				subtitle : {
					text : ''
				},
				plotOptions : {
					pie : {
						innerSize : 100,
						depth : 45,
						dataLabels : {
							enabled : false
						},
						showInLegend : true
					},

				},
				legend : {
					align : 'right',
					verticalAlign : 'top',
					y : 60,
					layout : 'vertical',
					padding : 3,
					itemMarginTop : 5,
					itemMarginBottom : 5,
				},
				series : [{
					name : 'Product',
					data : [['Trade', 1258963547889], ['CBB', cbbVal], ['Export', exportVal], ['Modern Trade', modernTradeVal]]
				}]
			});
		},
//tradeVal
		generateDriverTreeNode : function(responseObject, parentId) {
			var nodeInfo = responseObject;

			if (nodeInfo.level_no == 0) {

				$('#wrapper').append("<div class='root' data-action='zoom' id='" + nodeInfo.node_notation + "' formula='" + nodeInfo.formula + "'></div>");

				$('.root').append("<span class='label' id='" + nodeInfo.node_notation + "_span' formula='" + nodeInfo.formula + "' parent='" + nodeInfo.parent_node + "'> <span class='single-child'>" + nodeInfo.name + "</span> <div class='clear'></div><dl class='value-cnt actual-value value-red'><dt>Actual</dt><dd id='actualValue'>" + Math.ceil(parseInt(nodeInfo.actual_value)) + "</dd></dl><dl class='value-cnt plan-value'><dt>Plan</dt><dd id='planValue'>" + Math.ceil(parseInt(nodeInfo.planned_value)) + "</dd></dl><div class='change-revenue-value hidden-cnt'><input type='text' readonly value='" + Math.ceil(parseInt(nodeInfo.actual_value)) + "'</div></span>");

			} else {
				var className = "lv" + (nodeInfo.level_no + 1);

				$('#' + parentId).append("<span class='label' id='" + nodeInfo.node_notation + "_span' formula='" + nodeInfo.formula + "' parent='" + nodeInfo.parent_node + "'> <span class='multi-child'>" + nodeInfo.name + "</span> <div class='clear'></div><dl class='value-cnt actual-value value-red'><dt>Actual</dt><dd id='actualValue'>" + Math.ceil(parseInt(nodeInfo.actual_value)) + "</dd></dl><dl class='value-cnt plan-value'><dt>Plan</dt><dd id='planValue'>" + Math.ceil(parseInt(nodeInfo.planned_value)) + "</dd></dl><div class='change-revenue-value hidden-cnt'><input type='text' readonly value='" + Math.ceil(parseInt(nodeInfo.actual_value)) + "'></div></span>");
			}
			if (nodeInfo.has_children) {
				var className = "lv" + (nodeInfo.level_no + 1);
				if (nodeInfo.level_no == 0) {

					$('.root').append("<div class='branch " + className + "'></div>");

				} else {

					$('#' + parentId).append("<div class='branch " + className + "'></div>");

				}
				for (var i = 0; i < nodeInfo.sub_nodes.length; i++) {

					$('#' + parentId).children('.' + className).append("<div class='entry' id='" + nodeInfo.sub_nodes[i].node_notation + "'parent = '" + nodeInfo.sub_nodes[i].parent_node + "' formula='" + nodeInfo.formula + "'>");

					router.generateDriverTreeNode(nodeInfo.sub_nodes[i], nodeInfo.sub_nodes[i].node_notation);
				}
			} else {
				$('#' + nodeInfo.node_notation + "_span").children('.change-revenue-value').addClass('ediatble-input');

				$('#' + nodeInfo.node_notation + "_span").find("input").on('click', function() {

					var $this = $(this);

					$('#amount').val($this.val());
					amountStoredVal = $this.val();
					overlayBox('change_value');

					$("#change_value .save-btn").off("click").on("click", function() {

						$this.val($(".amount-container #amount").val());

						$(".closeBtn").trigger("click");

						var value = $this.val();

						var changedInputParentId = $this.closest(".entry").attr("id");

						// $this.closest(".entry").siblings(".entry").each(function(){
						// $this = $(this);
						// console.log($this.find("input").val());
						// });

						router.whatIfValue(value, changedInputParentId);

					});

				});

				// $('#'+nodeInfo.node_notation+"_span").children('.change-revenue-value').append("<input type='text' value='300'>");
				//
			}
		},
		whatIfValue : function(selfValue, nodeId) {
	
			var parentId = $("#" + nodeId).attr('parent');
			if (parentId != undefined) {
				console.log("Parent : " + parentId);
				console.log("Formula : " + $("#" + nodeId).attr('formula'));
				console.log("Self :" + nodeId);
				console.log(selfValue);
				var formula = $("#" + nodeId).attr('formula');
				formula = formula.replace(nodeId, selfValue);

				$('#' + nodeId).siblings(".entry").each(function() {
					$this = $(this);
					console.log($this.attr("id"));
					console.log($this.find("input").val());
					formula = formula.replace($this.attr("id"), $this.find("input").val());
				});
				console.log(formula);
				var finalValue = eval(formula);
				console.log(finalValue);
				console.log($('#' + parentId).find('#' + parentId + "_span input").first());
				$('#' + parentId).find('#' + parentId + "_span input").first().val(Math.ceil(finalValue));
				console.log($('#' + parentId).attr('class'));

				router.whatIfValue(finalValue, parentId);

			}
		},
		toPctFn : function(values){
			 var sum = 0;
			 for( var i = 0; i != values.length; ++i ) { 
				sum = sum + values[i]; 
			}
			var scale = 100/sum;
			return function( x ){ 
				return x*scale;
			};
		}
	});

	return AppRouter;
});
