requirejs.config({
	
	baseUrl : "js/",
	paths : {
		"jquery" : "libs/jquery-min",
		"jquery-ui":"libs/jquery-ui.min",
		"fg-menu":"libs/fg.menu",
		"backbone" : "libs/backbone-min-1.2.3",
		"underscore":"libs/underscore",
		"common":"libs/common",
		"jquerytree" : "libs/jquery.tree",
		"highcharts":"libs/highcharts",
		"jquerymigrate":"libs/jquery-migrate-1.2.1.min",
		"jquerytouch" :"libs/jquery.ui.touch-punch.min",
		"text" : "libs/plugin/text"
		/* "zoom" : "libs/plugin/zoom",
		"transition" : "libs/plugin/transition" */
	},
	shim : {
		"jquery" : {
			exports : "$"
		},
		"underscore" : {
			exports : "_"
		},
		"backbone" : {
			deps : ["jquery", "underscore"],
			exports : "Backbone"
		},

	},
	waitSeconds : 0
	
});
requirejs(["jquery", "app"], function($, App) {
	$(function() {
		App.initialize();
	});
}); 