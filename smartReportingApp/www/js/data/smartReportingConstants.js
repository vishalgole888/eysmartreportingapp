define([],function(){
	var SmartReportingConstants = {};
	
	SmartReportingConstants.URL = "http://smartreporting.intelliswift.in/";//"http://172.20.4.75:3000/";
	
	SmartReportingConstants.SECTORS_LIST_URL = SmartReportingConstants.URL + "getSectorList";
	SmartReportingConstants.INDUSTRY_LIST_URL = SmartReportingConstants.URL + "getIndustryList";
	SmartReportingConstants.CATEGORY_LIST_URL = SmartReportingConstants.URL +"getPerformanceIndicatorCategoryList";
	SmartReportingConstants.PERFORMANCE_METRIC_LIST_URL = SmartReportingConstants.URL + "getPerformanceIndicatorList";
	SmartReportingConstants.DIMENSIONS_URL = SmartReportingConstants.URL + "getDimensions";
	SmartReportingConstants.PERFORMANCE_ANALYSIS_URL = SmartReportingConstants.URL + "getPerformanceAnalysis";
	SmartReportingConstants.DRIVER_TREE = SmartReportingConstants.URL + "getDriverTree";
	return SmartReportingConstants;
});
