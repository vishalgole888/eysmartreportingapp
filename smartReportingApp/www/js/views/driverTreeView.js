define(["jquery", "backbone", "common", "highcharts", "jquerymigrate", "jquerytouch", "jquery-ui", "jquerytree", "text!views/templates/driver-tree1.html"], function($, Backbone, Common, HighCharts, JQueryMigrate, JQueryTouch, JQueryUI, JQueryTree, DriverTreeTemplate) {


	var selectedTab;
	
	var DriverTreeView = Backbone.View.extend({
		
		attributes : {
			'data-role' : 'page'
		},

		template : _.template(DriverTreeTemplate),

		render : function(eventName) {
			$(this.el).html(this.template(this.model));
			
			return this;
		},
		
		
		events : {
			"click .box-wrapper" : "selectSectors",

			"click .tabNav li a" : "tabChanged",
			
			"click .three-tab div" : "tabChanged",
			
			"click .prp-tab a" : "tabChanged"
		},

		selectSectors : function(event) {

		},
		tabChanged : function(event) {
			//alert("tabChanged");

			console.log($(event.target));

			var relation = $(event.target).attr('rel');
			if(relation=="undefined"){
				
				relation = $(event.target).children().attr("rel");
			}
			if (relation == "tab1") {
				$('#filterBox').css('display','block');
				console.log("Driver tree selected");
				console.log($('#line-dropdown').find('.menuSelection').attr('id'));
				console.log($('#channel-dropdown').find('.menuSelection').attr('id'));
				console.log($('#geography-dropdown').find('.menuSelection').attr('id'));
				console.log($('#period-dropdown').find('.menuSelection').text());
				router.getDriverTree($('#line-dropdown').find('.menuSelection').attr('id'), $('#channel-dropdown').find('.menuSelection').attr('id'), $('#geography-dropdown').find('.menuSelection').attr('id'), $('#period-dropdown').find('.menuSelection').text());
			} else if (relation == "tab2") {
				$('#filterBox').css('display','none');
				console.log("Reporting selected");
			} else if (relation == "tab3") {
				$('#filterBox').css('display','none');
				console.log("PerformanceAnalysis selected");
				router.performanceAnalysis();
			}

			if (selectedTab != $(event.target).attr('rel')) {
				selectedTab = $(event.target).attr('rel');
				console.log(relation);

				var tabNavigation = $(event.target).parents('.tabNav');
				console.log(tabNavigation);

				var resultCnt = $(event.target).parents('.tabNav').next('.tabResult');

				//console.log(tabNavigation.children().find('a.active').attr('rel'));
				tabNavigation.children().find('a').removeClass('active');
				tabNavigation.children().find('li').removeClass('activeli');
				$(event.target).addClass('active');
				$(event.target).parents('a').addClass('active');
				$(event.target).parents('li').addClass('activeli');

				if (resultCnt.children('div#' + relation).css('display') == 'none') {
					resultCnt.children('div').slideUp();
					resultCnt.children('div#' + relation).slideDown();
					resultCnt.children('div#' + relation).addClass('active');
					//graph_intialiation()
					//load_pie_chart()
				} else {

					resultCnt.children('div#' + relation).slideUp().removeClass('active');

				}
			}
			//location.href="#/dimensions/"+selectedTab;
		},
		getDriverTree : function(event) {
			// $('.tab1').trigger('click', function() {
			//
			// });
		},
		getReporting : function(event) {
			alert("Under development");
		},
		getPerformanceAnalysis : function(event) {
			// $('.tab3').trigger('click', function() {
			//
			// });
		}
	});
	return DriverTreeView;
});
