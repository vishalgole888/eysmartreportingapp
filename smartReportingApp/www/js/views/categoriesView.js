define(["jquery", "backbone", "text!views/templates/categories.html"],function($,Backbone,CategoriesTemplate){
	
	var CategoriesView = Backbone.View.extend({
		attributes : {
			'data-role' : 'page'
		},

		template : _.template(CategoriesTemplate),

		render : function(eventName) {
			$(this.el).html(this.template(this.model));
			return this;
		},
		
		events : {
			"click .box-wrapper" : "selectCategory",
		},
		
		selectCategory : function(event){
			console.log($(event.target));
			var selectedCategoryId;
			
			if(($(event.target).attr('class')!="box") || ($(event.target).attr('class')!="box-wrapper"))
			{
				console.log($(event.target).closest("div").attr('id'));
				selectedCategoryId = $(event.target).closest("div").attr('id');
				selectedCategoryName = $(event.target).closest("div").attr('name');
			}
			else{
				console.log($(event.target).attr('id'));
				selectedCategoryId = $(event.target).attr('id');
				selectedCategoryName = $(event.target).attr('name');
			};
			location.href="#/performanceMetrics/"+selectedCategoryId;
			//router.performanceMetrics(selectedCategoryId);
			return false;
		}
	});
	return CategoriesView;
});
