define(["jquery", "backbone", "text!views/templates/performanceMetric.html"],function($,Backbone,PerformanceMetricsTemplate){
	
	var PerformanceMetricsView = Backbone.View.extend({
		attributes : {
			'data-role' : 'page'
		},

		template : _.template(PerformanceMetricsTemplate),

		render : function(eventName) {
			$(this.el).html(this.template(this.model));
			return this;
		},
		
		events : {
			"click .box" : "selectPerformanceMetrics",
		},
		
		selectPerformanceMetrics : function(event){
			console.log($(event.target));
			var selectedMetricId;
			
			if(($(event.target).attr('class')!="box") || ($(event.target).attr('class')!="box-wrapper"))
			{
				console.log($(event.target).closest("div").attr('id'));
				selectedMetricId = $(event.target).closest("div").attr('id');
				selectedPerformanceMetrics = $(event.target).closest("div").attr('name');
			}
			else{
				console.log($(event.target).attr('id'));
				selectedMetricId = $(event.target).attr('id');
				selectedPerformanceMetrics = $(event.target).attr('name');
			}
			location.href="#/operations/"+selectedMetricId;
			//router.operations(selectedMetricId);
			return false;
		}
	});
	return PerformanceMetricsView;
});