define(["jquery", "backbone", "text!views/templates/sectors.html"],function($,Backbone,SectorsTemplate){
	
	var SectorsView = Backbone.View.extend({
		attributes : {
			'data-role' : 'page'
		},

		template : _.template(SectorsTemplate),

		render : function(eventName) {
			$(this.el).html(this.template(this.model));
			return this;
		},
		
		events : {
			"click .box-wrapper" : "selectSectors",
		},
		
		selectSectors : function(event){
			console.log($(event.target));
			var selectedSectorId;
			if(($(event.target).attr('class')!="box") || ($(event.target).attr('class')!="box-wrapper"))
			{
				console.log($(event.target).closest("div").attr('id'));
				selectedSectorId = $(event.target).closest("div").attr('id');
			}
			else{
				console.log($(event.target).attr('id'));
				selectedSectorId = $(event.target).attr('id');
			}
			location.href="#/industries/"+selectedSectorId;
			//router.industries(selectedSectorId);
			return false;
		}
	});
	return SectorsView;
});
