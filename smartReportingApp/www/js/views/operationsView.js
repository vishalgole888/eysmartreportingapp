define(["jquery", "backbone", "text!views/templates/operations.html"],function($,Backbone,OperationsTemplate){
	
	var OperationsView = Backbone.View.extend({
		attributes : {
			'data-role' : 'page'
		},

		template : _.template(OperationsTemplate),

		render : function(eventName) {
			$(this.el).html(this.template(this.model));
			return this;
		},
		
		events : {
			"click .box" : "selectOperation",
		},
		
		selectOperation : function(event){
			var selectedOperation;
			console.log($(event.target).parent().text());
			console.log($(event.target).next("h3").text());
			console.log($(event.target).find("h3").text());
			if(($(event.target).attr('class')=="box") || ($(event.target).attr('class')=="box-wrapper"))
			{
				//console.log($(event.target).find("h3"));
				selectedOperation = $(event.target).find("h3").text().trim();
			}
			else if($(event.target).attr('id') == 'iconHolder'){
				selectedOperation = $(event.target).next("h3").text().trim();
			}
			else{
				console.log($(event.target).parent().text());
				selectedOperation = $(event.target).parent().text().trim();
			};
			console.log("SelectedOperation : "+ selectedOperation);
			location.href="#/dimensions/"+selectedOperation;
			return false;
		}
	});
	return OperationsView;
});