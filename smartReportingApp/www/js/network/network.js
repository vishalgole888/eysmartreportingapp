define(["require", "jquery", "backbone",], function(require, $, Backbone, AppData) {

	var sendRequest = function(data, OnSuccess, OnError) {

		var constants = require("data/smartReportingConstants");
		
		console.log("Network : sending Request..." + data.url);
		console.log("Network : data..." + JSON.stringify(data.postData));

		$.ajax({
			type : 'POST',
			url : data.url,
			data : JSON.stringify(data.postData),
			contentType : "application/json",
			beforeSend: function (request)
            {
                request.setRequestHeader("Access-Control-Allow-Origin", "*");
            },
			success : OnSuccess,
			error : OnError,
			timeout : 30000,
			async : true
		});
	};
	return {
		sendRequest : sendRequest,
	};
});

