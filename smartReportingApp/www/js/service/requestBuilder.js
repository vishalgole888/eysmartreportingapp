define([], function() {

	var userToken = "8EsQEZwEgbxgAZ3tj8xE";
	//window.localStorage.getItem("userToken");

	var getSectorsRequest = function() {
		var getSectorsJson = eval({
			"tokenId" : userToken
		});
		return getSectorsJson;
	};

	var getIndustriesRequest = function(sectorId) {
		var getIndustriesJson = eval({
			"tokenId" : userToken,
			"sectorId" : sectorId
		});
		return getIndustriesJson;
	};

	var getPerformanceMetricCategory = function(industryId) {

		var getCategoryJson = eval({
			"tokenId" : userToken,
			"industryId" : industryId
		});
		return getCategoryJson;
	};

	var getPerformanceMetric = function(categoryId) {
		var getPerformanceMetricJson = eval({
			"tokenId" : userToken,
			"indicatorCategoryId" : categoryId
		});
		return getPerformanceMetricJson;
	};

	var getDimensions = function(performanceMetricId, operationId) {
		var getDimensionsJson = eval({
			"tokenId" : userToken,
			"performanceMetricId" : performanceMetricId,
			"operationsId" : operationId,
		});
		return getDimensionsJson;
	};

	var getPerformanceAnalysis = function(performanceMetricId) {
		var getPerformanceAnalysisJson = eval({
			"tokenId" : userToken,
			"performanceMetricId" : performanceMetricId
		});
		return getPerformanceAnalysisJson;
	};
	var getDriverTree = function(performanceMetricId,productCatergory, distributionChannel, geography, period) {
		var getDriverTreeJson = eval({

			"tokenId" : userToken,
			"performanceMetricId" : performanceMetricId,
			"product_category" : productCatergory,
			"distribution_channel" : distributionChannel,
			"country" : geography,
			"year" : period

		});
		return getDriverTreeJson;
	};
	return {
		getSectorsRequest : getSectorsRequest,
		getIndustriesRequest : getIndustriesRequest,
		getPerformanceMetricCategory : getPerformanceMetricCategory,
		getPerformanceMetric : getPerformanceMetric,
		getDimensions : getDimensions,
		getPerformanceAnalysis : getPerformanceAnalysis,
		getDriverTree : getDriverTree
	};
});
