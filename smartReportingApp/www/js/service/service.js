define(["network/network", "data/smartReportingConstants"], function(Network, Constants) {

	var getSectors = function(data, sucessAction, errorAction) {
		var requestObject = new Object();
		requestObject.postData = data, requestObject.url = Constants.SECTORS_LIST_URL, Network.sendRequest(requestObject, sucessAction, errorAction);
	};

	var getIndustries = function(data, sucessAction, errorAction) {
		var requestObject = new Object();
		requestObject.postData = data, requestObject.url = Constants.INDUSTRY_LIST_URL, Network.sendRequest(requestObject, sucessAction, errorAction);
	};
	var getCategories = function(data, sucessAction, errorAction) {
		var requestObject = new Object();
		requestObject.postData = data, requestObject.url = Constants.CATEGORY_LIST_URL, Network.sendRequest(requestObject, sucessAction, errorAction);
	};

	var getPerformanceMetrics = function(data, sucessAction, errorAction) {
		var requestObject = new Object();
		requestObject.postData = data, requestObject.url = Constants.PERFORMANCE_METRIC_LIST_URL, Network.sendRequest(requestObject, sucessAction, errorAction);
	};

	var getDimensions = function(data, sucessAction, errorAction) {
		var requestObject = new Object();
		requestObject.postData = data, requestObject.url = Constants.DIMENSIONS_URL, Network.sendRequest(requestObject, sucessAction, errorAction);
	};

	var getPerformanceAnalysis = function(data, successAction, errorAction) {
		var requestObject = new Object();
		requestObject.postData = data, requestObject.url = Constants.PERFORMANCE_ANALYSIS_URL, Network.sendRequest(requestObject, successAction, errorAction);
	};

	var getDriverTree = function(data, successAction, errorAction) {
		var requestObject = new Object();
		requestObject.postData = data, requestObject.url = Constants.DRIVER_TREE, Network.sendRequest(requestObject, successAction, errorAction);
	};

	return {
		getSectors : getSectors,
		getIndustries : getIndustries,
		getCategories : getCategories,
		getPerformanceMetrics : getPerformanceMetrics,
		getDimensions : getDimensions,
		getPerformanceAnalysis : getPerformanceAnalysis,
		getDriverTree : getDriverTree
	};
});
