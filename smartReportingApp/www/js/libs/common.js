/*=======================================
 Credits
 File Name:- common.js
 Developed by Pratap Tondwalkar
 all rights reserved for Intelliswift Software India Pvt. Ltd.

 Component Incude :-
 Events
 1) Document ready
 2) Document load
 3) Document resize

 Functions
 1) take me to top (scroll to top)
 2) mobile navigation

 Note:-
 need to include jQuery library above 1.8

 You can inculde more component just make sure to add those in above list which is help for while maintencnce.

 ========================================*/

/*=========================== Document resize funtion start here ======================*/
$(window).resize(function() {
	alignments();
	dropdown_function();
});
/*=========================== Document resize funtion start here ======================*/

/*=========================== Document Load funtion start here ======================*/
$(window).load(function() {
	$('.loader').fadeOut();
	dropdown_function();
})
/*=========================== Document Load funtion end here ======================*/

/*=========================== Document ready funtion start here ======================*/
$(function() {
	dropdown_function();
	/*----- file upload name display script end -------*/
	$(function() {
		$('.animated-label-form .animate-label .input-group').click(function() {
			$(this).find('input').focus();
			$(this).find('label').addClass('active');
		});
		$('.animate-label .input-group input,.animate-label .input-group textarea,.animate-label .input-group select').blur(function() {
			if (this.value.length > 0) {
				return false;
			} else {
				$(this).next('label').removeClass('active');
			}
		});
		$('.input-group').on('focus', 'input,select,textarea', function() {
			$(this).next('label').addClass('active');
		});

		$('.input-group textarea, .input-group input, .input-group select').each(function() {
			if (this.value.length > 0) {
				var div = $(this).next('label').addClass('active');
			}
		});

		$('.input-type-select span').click(function() {
			$(this).parents('.input-type-select').find('select').trigger('click');
		});

	});

	$(function() {

		$('.input-type-select').click(function() {
			$(this).find('select').focus();
			$(this).addClass('active-select');
		});
		$('.input-type-select select').blur(function() {
			if (this.value.length > 0) {
				return false;
			} else {
				$(this).removeClass('active-select');
			}
		});
		$('.input-type-select').on('focus', 'select', function() {
			$(this).addClass('active-select');
		});

		$('.input-type-select select').each(function() {
			if (this.value.length > 0) {
				var div = $(this).addClass('active-select');
			}
		});

	});

	/*=========================== Back to top Function Start =====================*/
	$('body').append('<div class="scrollTop"><a href="javascript:void(0)"></a></div>');
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			$('.scrollTop').fadeIn();
		} else {
			$('.scrollTop').fadeOut();
		}
	});
	$(document).on('click', '.scrollTop a', function() {
		$('body,html').animate({
			scrollTop : 0
		}, 800);
	});

	/*=========================== Back to top Function End ======================*/

	/*=========================== Mobile navigation Start ======================*/
	$('.toggle-btn').click(function() {
		if ($('.wrapper').hasClass('opened')) {
			$('.wrapper').removeClass('opened');
		} else {
			$('.wrapper').addClass('opened');
		}

	});
	$('.has-sub-menu').click(function() {
		var target = $(this).find('.sub-menu');
		var trigger = $(this);
		if (trigger.hasClass('opened')) {
			trigger.removeClass('opened');
		} else {
			trigger.addClass('opened');
		}

	});
	/*=========================== Mobile navigation end ======================*/

});
/*=========================== Document ready funtion end here ======================*/

/*======================= Overlay function =======================*/
var animationIn,
    target,
    animationOut;
function overlayBox(popupID) {
	
	target = $('#' + popupID)
	console.log(target)
	animationIn = target.attr('data-animation-in');
	animationOut = target.attr('data-animation-out');
	if ( typeof (animationIn) == 'undefined' || animationIn === '(an empty string)' || animationIn === null || animationIn === '') {
		animationIn = 'zoomIn';
	}
	if ( typeof (animationOut) == 'undefined' || animationOut === '(an empty string)' || animationOut === null || animationOut === '') {
		animationOut = 'zoomOut';
	}
	$('body').append('<div class="overlay-bg"></div>')
	target.find('.overlay-header').append('<div class="closeBtn"><img src="img/icon/close-icon.png" alt=""/></div>');
	target.css('visibility', 'visible').find('.overlay-box').addClass('animated').addClass(animationIn).css('display', 'table');
	$(document).on('click', '.closeBtn', function() {
		$('.overlay').find('.overlay-box').removeClass('animated').removeClass(animationIn).addClass('animated ' + animationOut);
		$('body .overlay-bg').fadeOut(1000, function() {
			$(this).remove();
			$('.overlay').css('visibility', 'hidden').find('.overlay-box').removeClass('animated').removeClass(animationIn).removeClass(animationOut).css('display', 'none');
		});
	});
}

/*===========================  Overlay function end  ========================*/

$(function() {
	$(document).on('click', '.category-button a', function() {
		var target_container = $(this).attr('data-target');
		$('.pie-graph-container').hide();
		$('.' + target_container).show();
		router.updatePieChart();
		//load_pie_chart()
	});

	$(document).on('click', '.what-if-analysis', function() {
		if ($('.change-revenue-value').hasClass('hidden-cnt')) {
			$('.change-revenue-value').removeClass('hidden-cnt')
		} else {
			$('.change-revenue-value').addClass('hidden-cnt')
		}
	});
	/*jQuery tabs */
	/*script for append usefull element*/
	$('.tabNav li').each(function() {
		/*$(this).css({
		 'width' : (100 / ( $('li:last-child').index('li') + 1 ) ) +  '%'
		 })*/
		var tabContent = $(this).html();
		var relation = $(this).find('a').attr('rel')
		var resultCnt = $(this).parents('.tabNav').next('.tabResult');
		resultCnt.children('div#' + relation).prepend('<div class="mobile-menu">' + tabContent + '</div>')

	})
	/*script for mobile navigation */
	//$(document).on('click','.mobile-menu',function(){
	//		if($(this).next('.content').css('display') == 'none')
	//		{
	//			$(this).closest('.tabResult').find('.content').slideUp();
	//			$(this).next('.content').slideDown();
	//		}
	//		else
	//		{
	//			$('.tabResult .tabBx .content').slideUp();
	//		}
	//	})
	/*script for desktop navigation */
	$(document).on('click', '.tabNav li a', function() {
		// var relation = $(this).attr('rel')
		// var tabNavigation = $(this).parents('.tabNav')
		// var resultCnt = $(this).parents('.tabNav').next('.tabResult');
// 
		// tabNavigation.children().find('a').removeClass('active');
		// tabNavigation.children().find('li').removeClass('activeli')
		// $(this).addClass('active');
		// $(this).parents('li').addClass('activeli');
// 
		// if (resultCnt.children('div#' + relation).css('display') == 'none') {
			// resultCnt.children('div').slideUp();
			// resultCnt.children('div#' + relation).slideDown();
			// resultCnt.children('div#' + relation).addClass('active');
			// //graph_intialiation()
			// //load_pie_chart()
		// } else {
			// resultCnt.children('div#' + relation).slideUp().removeClass('active');
			// ;
		// }
	})
})
/*jQuery tabs end */

$(".username-text").click(function() {
	$("#remove-username").show();
	$("#show-password").hide();
});
$(".password-text").click(function() {
	$("#remove-username").hide();
	$("#show-password").show();
});

function alignments() {
	var winW = $(window).width();
	var windH = $(window).height();

	var hdrH = $('.header').outerHeight();
	$('.content-area .container').css('min-height', windH - hdrH - 25);

}

function graph_intialiation() {
	$('#revenue-line-graph').highcharts({
		credits : {
			enabled : false
		},
		chart : {
			zoomType : 'xy'
		},
		title : {
			text : ''
		},
		xAxis : [{
			categories : ['Mar, 11', 'Mar, 12', 'Mar, 13', 'Mar, 14', 'Mar, 15'],
			crosshair : true
		}],
		yAxis : [{// Primary yAxis
			labels : {

				format : '{value}.00',
				style : {
					color : Highcharts.getOptions().colors[1]
				}
			},
			title : {
				text : '',
				style : {
					color : Highcharts.getOptions().colors[1]
				}
			}
		}, {// Secondary yAxis
			title : {
				text : '',
				style : {
					color : Highcharts.getOptions().colors[1]
				}
			},
			labels : {
				format : '{value}.00',
				style : {
					color : Highcharts.getOptions().colors[1]
				}
			},
			opposite : true
		}],
		tooltip : {
			shared : true
		},
		legend : {
			backgroundColor : '#fff',
			borderColor : '#ddd',
			borderWidth : 1,
			itemDistance : 50,
			fontWeight : 'bold'
		},
		series : [{
			name : 'Actual',
			type : 'column',
			yAxis : 1,
			data : [49.9, 71.5, 106.4, 129.2, 144.0],
			tooltip : {
				valueSuffix : ' mm'
			},
			color : "#808080"

		}, {
			name : 'Plans',
			type : 'spline',
			data : [7.0, 6.9, 9.5, 14.5, 18.2],
			tooltip : {
				valueSuffix : '°C'
			},
			color : "#ffd200"
		}]
	});

}

function load_pie_chart() {
	$('#revenue-pie-business-graph').highcharts({
		credits : {
			enabled : false
		},
		chart : {
			type : 'pie',
			options3d : {
				enabled : false,
				alpha : 45
			}
		},
		title : {
			text : ''
		},
		subtitle : {
			text : ''
		},
		plotOptions : {
			pie : {
				innerSize : 100,
				depth : 45,
				dataLabels : {
					enabled : false
				},
				showInLegend : true
			},

		},
		legend : {
			align : 'right',
			verticalAlign : 'top',
			y : 60,
			layout : 'vertical',
			padding : 3,
			itemMarginTop : 5,
			itemMarginBottom : 5,
		},
		series : [{
			name : 'Product',
			data : [['Product 1', 8], ['Product 2', 3], ['Product 3', 1], ['Product 4', 6], ['Product 5', 8]]
		}]
	});
	$('#revenue-pie-distribustion-graph').highcharts({
		credits : {
			enabled : false
		},
		chart : {
			type : 'pie',
			options3d : {
				enabled : false,
				alpha : 45
			}
		},

		title : {
			text : ''
		},
		subtitle : {
			text : ''
		},
		plotOptions : {
			pie : {
				innerSize : 100,
				depth : 45,
				dataLabels : {
					enabled : false
				},
				showInLegend : true
			},

		},
		legend : {
			align : 'right',
			verticalAlign : 'top',
			y : 60,
			layout : 'vertical',
			padding : 3,
			itemMarginTop : 5,
			itemMarginBottom : 5,
		},
		series : [{
			name : 'Product',
			data : [['Product 1', 5], ['Product 2', 7], ['Product 3', 3], ['Product 4', 6], ['Product 5', 4]]
		}]
	});
}

function dropdown_function() {
	//alert("Hi");
	$('#line-dropdown').menuize({
		content : $('#line-dropdown').next().html(),
		crumbDefaultText : ' '
	});
	$('#channel-dropdown').menuize({
		content : $('#channel-dropdown').next().html(),
		crumbDefaultText : ' '
	});
	$('#geography-dropdown').menuize({
		content : $('#geography-dropdown').next().html(),
		crumbDefaultText : ' '
	});
	$('#period-dropdown').menuize({
		content : $('#period-dropdown').next().html(),
		crumbDefaultText : ' '
	});
	
}